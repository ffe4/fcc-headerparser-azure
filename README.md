# Request Header Parser Microservice

Azure Functions implementation of the Request Header Parser Microservice API project from freeCodeCamp.org.

## Deploying using CLI

You will need the [Azure CLI](https://github.com/Azure/azure-cli) and [Azure Functions Core Tools](https://github.com/Azure/azure-functions-core-tools). See links for installation instructions.

```shell
# create resource group
az group create --name <resource-group-name> --location <location>
# create function app
az group deployment create \
    --resource-group <resource-group-name> \
    --template-file azuredeploy.json \
    --parameters appName=<function-app-name> storageName=<storage-account-name>
# publish function
cd app
func azure functionapp publish <function-app-name> --build remote
```
