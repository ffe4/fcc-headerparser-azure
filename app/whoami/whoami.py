"""Azure Functions implementation of freeCodeCamp API Project: Request Header Parser Microservice"""
import json
import logging

import azure.functions as func


def parse_header(req: func.HttpRequest) -> func.HttpResponse:
    """Return response containing IP address, language and operating system for client browser."""

    logging.info('Python HTTP trigger function processed a request.')

    return func.HttpResponse(json.dumps({
        'ip': req.headers.get('x-forwarded-for'),
        'language': req.headers.get('accept-language'),
        'software': req.headers.get('user-agent'),
        }))
